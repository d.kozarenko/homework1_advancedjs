"use strict";

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(value) {
    this._name = value;
  }
  get age() {
    return this._age;
  }
  set age(value) {
    this._age = value;
  }
  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value;
  }
}

const employee = new Employee("Ben", 25, 500000);

console.log(employee);

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get salary() {
    return this._salary * 3;
  }
}

const programmer1 = new Programmer("John Carmack", 19, 760090, [
  "English",
  "Russian",
]);

const programmer2 = new Programmer("Bill Gosper", 78, 100000, [
  "Ukrainian",
  "Japanese",
]);

const programmer3 = new Programmer("Richard Greenblatt", 76, 32232, [
  "English",
  "Romanian",
]);

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);
